
package com.example.arcklab;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.Objects;


public class NetUtil {
    /**
     * Check Http response
     * all response contain here are not success response
     *
     * @param responseCode
     * @return
     */
    public static boolean isValidResponseCode(int responseCode) {
        return Arrays.asList(500, 503, 504, 404).contains(responseCode);

    }


    public static boolean isInternetAvailable() {

        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean isInternetConnected() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }
    }


    public static boolean isOnline(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }


    public static boolean isNetConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        assert cm != null;
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnectedOrConnecting()) {

            return true;

        } else {

            return false;

        }

    }

    /**
     * Check connectivity availability
     *
     * @param mContext
     * @return
     */
    public static boolean isNetworkConnected(Context mContext) {
        try {
            NetworkInfo activeNetwork = ((ConnectivityManager) Objects.requireNonNull(mContext.getSystemService(Context.CONNECTIVITY_SERVICE))).getActiveNetworkInfo();
            return activeNetwork != null && activeNetwork.isConnected();
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean haveInternet(Context ctx) {

        NetworkInfo info = (NetworkInfo) ((ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        if (info == null || !info.isConnected()) {
            return false;
        }
        if (info.isRoaming()) {
            // here is the roaming option you can change it if you want to
            // disable internet while roaming, just return false
            return false;
        }
        return true;
    }

    public static String getNetworkInfo(Activity activity) {
        ConnectivityManager conMan = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        String state = "";
        assert conMan != null;
        if (conMan.getNetworkInfo(0).getState()
                == NetworkInfo.State.CONNECTED ||
                conMan.getNetworkInfo(0).getState()
                        == NetworkInfo.State.CONNECTING) {

            state = "Mobile";
            return state;

            //mobile
        } else if (conMan.getNetworkInfo(1).getState()
                == NetworkInfo.State.CONNECTED ||
                conMan.getNetworkInfo(1).getState() ==
                        NetworkInfo.State.CONNECTING) {
            //wifi

            state = "Wifi";
            return state;

        }
        return state;
    }
}
