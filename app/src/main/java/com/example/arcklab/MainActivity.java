package com.example.arcklab;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    private WebView webView;
    private RelativeLayout relativeLayoutNoNetwork;
    private Button btnRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = findViewById(R.id.webView);
        relativeLayoutNoNetwork = findViewById(R.id.relative_lyt_no_network);
        btnRetry = findViewById(R.id.btn_failed_retry);
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onResume() {
        super.onResume();

        try {

            if (NetUtil.isNetConnected(MainActivity.this)) {
                relativeLayoutNoNetwork.setVisibility(View.GONE);
                webView.setVisibility(View.VISIBLE);
//            webView.loadUrl("http://cumilla-central.arcklab.com/auth/login");
//                webView.loadUrl("http://sylhet-central.arcklab.com/");
//                webView.loadUrl("http://barishal-central.arcklab.com/");
//                webView.loadUrl("http://latachpalimodelgps.arcklab.com/");
//                webView.loadUrl("http://bhola-central.arcklab.com/");
//                webView.loadUrl("http://kachiahgps.arcklab.com/");
//                webView.loadUrl("http://choaranondogps.arcklab.com/");
//                webView.loadUrl("http://wcgs-hajigonj.arcklab.com/");
//                webView.loadUrl("http://saifurs-central.arcklab.com");
//                webView.loadUrl("http://cumilla-central.arcklab.com/");
                webView.loadUrl("http://dinajpur-central.arcklab.com/auth/login");
                webView.getSettings().setJavaScriptEnabled(true);
                webView.setWebViewClient(new WebViewClient() {

                    @Override
                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                linearLayout.setVisibility(View.VISIBLE);
                        super.onPageStarted(view, url, favicon);
                    }

                    @Override
                    public void onPageFinished(WebView view, String url) {
//                linearLayout.setVisibility(View.GONE);
//                refreshLayout.setRefreshing(false);
                        super.onPageFinished(view, url);
//                myCurrentUrl = url;
                    }
                });

                webView.setWebChromeClient(new WebChromeClient() {


                    @Override
                    public void onProgressChanged(WebView view, int newProgress) {
                        super.onProgressChanged(view, newProgress);
//                progressBar.setProgress(newProgress);
                    }

                    @Override
                    public void onReceivedTitle(WebView view, String title) {
                        super.onReceivedTitle(view, title);
//                getSupportActionBar().setTitle(title);

                    }

                    @Override
                    public void onReceivedIcon(WebView view, Bitmap icon) {
                        super.onReceivedIcon(view, icon);
//                imageView.setImageBitmap(icon);
                    }
                });

            } else {
                relativeLayoutNoNetwork.setVisibility(View.VISIBLE);
                webView.setVisibility(View.GONE);
                btnRetry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (NetUtil.isNetConnected(MainActivity.this)) {
                            relativeLayoutNoNetwork.setVisibility(View.GONE);
                            webView.setVisibility(View.VISIBLE);
//                        webView.loadUrl("http://cumilla-central.arcklab.com/auth/login");
//                            webView.loadUrl("http://sylhet-central.arcklab.com/");
//                            webView.loadUrl("http://barishal-central.arcklab.com/");
//                            webView.loadUrl("http://latachpalimodelgps.arcklab.com/");
//                            webView.loadUrl("http://bhola-central.arcklab.com/");
//                            webView.loadUrl("http://kachiahgps.arcklab.com/");
//                            webView.loadUrl("http://choaranondogps.arcklab.com/");
//                            webView.loadUrl("http://wcgs-hajigonj.arcklab.com/");
//                            webView.loadUrl("http://saifurs-central.arcklab.com");
//                            webView.loadUrl("http://cumilla-central.arcklab.com/");
                            webView.loadUrl("http://dinajpur-central.arcklab.com/auth/login");
                            webView.getSettings().setJavaScriptEnabled(true);
                            webView.setWebViewClient(new WebViewClient() {

                                @Override
                                public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                linearLayout.setVisibility(View.VISIBLE);
                                    super.onPageStarted(view, url, favicon);
                                }

                                @Override
                                public void onPageFinished(WebView view, String url) {
//                linearLayout.setVisibility(View.GONE);
//                refreshLayout.setRefreshing(false);
                                    super.onPageFinished(view, url);
//                myCurrentUrl = url;
                                }
                            });

                            webView.setWebChromeClient(new WebChromeClient() {

                                @Override
                                public void onProgressChanged(WebView view, int newProgress) {
                                    super.onProgressChanged(view, newProgress);
//                progressBar.setProgress(newProgress);
                                }

                                @Override
                                public void onReceivedTitle(WebView view, String title) {
                                    super.onReceivedTitle(view, title);
//                getSupportActionBar().setTitle(title);

                                }

                                @Override
                                public void onReceivedIcon(WebView view, Bitmap icon) {
                                    super.onReceivedIcon(view, icon);
//                imageView.setImageBitmap(icon);
                                }
                            });

                        }
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack())
            webView.goBack();
        else
            finish();
    }
}
